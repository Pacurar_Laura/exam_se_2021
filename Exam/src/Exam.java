import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Exam {

    //S1 - diagram
    /*Class U {
        public void p();
    }

    Class I extends U {
        private long t;

        public void f();
        public K k = new K();
    }

    Class N {
        public N n = new I();
    }

    Class K {
        public M m;
        public K(M m)
        {
            this.m = new M;
        }
    }

    Class J {
        public i(I I);
    }

    Class L {
        public void metA();
    }

    Class M {
        public void metB();
    }*/

    //S2
    public static class GUI extends JFrame implements ActionListener {
        private JTextField textField1;
        private JPanel panel1;
        private JButton button1;

        public GUI(){
            setLayout(new FlowLayout());
            setSize(600, 600);
            textField1= new JTextField(8);
            add(textField1);
            button1= new JButton("Press");
            add(button1);
            button1.addActionListener(this);
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setVisible(true);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String reverse=textField1.getText();
            StringBuilder stringBuilder=new StringBuilder();
            stringBuilder.append(reverse);
            stringBuilder.reverse();
            System.out.println(stringBuilder);
        }

        public static void main(String[] args) {
            GUI gui=new GUI();
            gui.setVisible(true);
        }
    }




}
